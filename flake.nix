{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    vitasdk.url = "github:sleirsgoevy/vitasdk.nix";
    vitasdk.inputs.nixpkgs.follows = "nixpkgs";
    fenix.url = "github:nix-community/fenix";
    fenix.inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = { self, nixpkgs, vitasdk, fenix }: let
    system = "x86_64-linux";
    fenixPkgs = fenix.packages.${system};
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    devShell.${system} = pkgs.mkShell {
      packages = with pkgs; [
        cmake
        (fenixPkgs.complete.withComponents [
          "cargo"
          "clippy"
          "rust-src"
          "rustc"
          "rustfmt"
        ])
      ];
      inputsFrom = [ vitasdk.packages.${system} ];
      env.VITASDK = vitasdk.packages.${system}.vitasdk;
    };
  };
}
